# classical approach
336: original excluding Concrete  (logscale (0.0001, 1.4, 20)
337: used PGA list from 0.01
338: used PGA list (appending 0.0001, 0.001)
339: used PGA logscale (0.0001, 1.4, 10)
340: used PGA logscale (0.0001, 1.4, 30)

342: removed site_model, used PGA logscale (0.0001, 2.0, 20)

351: used linear scale, original vulnerability
352: used linear scale, vulnerability with higher resolution

353: log scale, vulnerability with higher resolution
354: log scale, original vulnerability

356: highest resolution scale and vulnerability

# event based 
20887943: linear scale, vulnerability with highest resolution
20970871: linear scale, modified vulnerabiity with highest resolution, 500K ses

# hazards
event_based: 20961255.gadi-pbs.csv
classical: 20962694.gadi-pbs.csv


