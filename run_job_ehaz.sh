#PBS -M hyeuk.ryu@ga.gov.au
#PBS -m e
#PBS -P y57
#PBS -q hugemem 
#PBS -l walltime=05:00:00
#PBS -l ncpus=48
#PBS -l mem=1470GB
#PBS -l wd
#PBS -N erisk
#PBS -l jobfs=500GB
#PBS -l storage=scratch/y57
#PBS -l other=hyperthread

export OQ_DATADIR=$PBS_JOBS
OUTPUT_PATH="$HOME/Projects/EventBasedRisk/output_erisk"
mkdir -p $OUTPUT_PATH

module load openquake/3.11.2
oq-ini.all.sh
oq engine --run job_ehaz.ini >&  $OUTPUT_PATH/$PBS_JOBID.log

oq show oqparam >& $OUTPUT_PATH/$PBS_JOBID.ini
#oq engine --export-output 1 $OUTPUT_PATH 
#oq engine --export-output 2 $OUTPUT_PATH 
#oq engine --export-output 3 $OUTPUT_PATH 
oq engine --export-output 4 $OUTPUT_PATH 
#oq engine --export-output 5 $OUTPUT_PATH 

thefiles=$(ls $OUTPUT_PATH/*_1.csv)
for thefile in $thefiles
    do
        beforedot=${thefile%_1.*}
        echo $beforedot
        newfile=${beforedot}_${PBS_JOBID}.csv
        echo $newfile
        mv $thefile $newfile
    done

#oq engine --list-outputs 1
# aggregate event losses: losses_by_event
#oq engine --export-output 5 $OUTPUT_PATH 
# average asset losses
# avg_losses-mean
# report.rst
#oq engine --export-output 3 $OUTPUT_PATH 
#oq engine --export-output 4 $OUTPUT_PATH 
# total loss curves
# tot_curves-rlzs.csv
#oq engine --export-output 9 $OUTPUT_PATH 
# tot_curves-stats.csv
#oq engine --export-output 10 $OUTPUT_PATH 
#oq engine --export-output 11 $OUTPUT_PATH 
# tot_losses-mean.csv
#oq engine --export-output 12 $OUTPUT_PATH 
# remove oqdata
rm -rf .oqdata.$PBS_JOBID.gadi-pbs
oq-end.sh
